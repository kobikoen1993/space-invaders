import pygame
pygame.init()

from SI_setting import *

class Start_game:
    def before_start(self,start,running,screen):
        while start:
            start = pygame.image.load('start.bmp').convert()
            screen.blit(start,(0,0))
            pygame.display.flip()
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    quit()
                elif event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_SPACE:
                        start = False
                        running = True

    def start_game(self):
        self.background = pygame.image.load('background.bmp').convert()

    def again(self,screen):
        screen.blit(self.background, (0, 0))

