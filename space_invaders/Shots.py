import pygame
pygame.init()

from SI_setting import *


class Shots(pygame.sprite.Sprite):
    def __init__(self, image, x, y):
        pygame.sprite.Sprite.__init__(self)
        self.x = x
        self.y = y
        self.image = image
        self.rect = image.get_rect(center=(x,y))

    def update(self, to):
        self.rect.y += to
        if self.rect.y >800 or self.rect.y < 0:
            self.kill()

