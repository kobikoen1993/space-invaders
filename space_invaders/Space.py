import pygame
pygame.init()

from SI_setting import *

class Space(pygame.sprite.Sprite):
    def __init__(self, space_image, self_q, self_w):
        pygame.sprite.Sprite.__init__(self)
        self.q = self_q
        self.w = self_w
        self.image = space_image
        self.rect = self.image.get_rect(center=(self.q,self.w))

    def update(self,r,s,count):
        self.rect.x += r
        self.count = count
        if self.count % 100 == 0:
            self.rect.y += s

