import pygame
pygame.init()

from SI_setting import *


class Ship(pygame.sprite.Sprite):
    def __init__(self, ship_x):
        pygame.sprite.Sprite.__init__(self)
        self.x = ship_x
        self.image =  pygame.image.load('ship.bmp')
        self.y = 800
        self.rect = self.image.get_rect(center=(self.x,self.y))

    def update(self):
        keys = pygame.key.get_pressed()
        if keys[pygame.K_LEFT] and self.rect.x > 0:
            self.rect.x -= 2
        elif keys[pygame.K_RIGHT] and self.rect.x < 1690:
            self.rect.x += 2

